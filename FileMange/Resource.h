//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FileMange.rc
//
#define IDC_MYICON                      2
#define IDD_FILEMANGE_DIALOG            102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDI_FILEMANGE                   107
#define IDI_SMALL                       108
#define IDC_FILEMANGE                   109
#define IDR_MAINFRAME                   128
#define IDD_DOITENTHEODINHDANG          129
#define IDD_DOITENTHEONGAY              130
#define IDR_MENU1                       131
#define IDC_EDIT3                       1004
#define IDC_EDIT4                       1005
#define IDC_RADIO3                      1006
#define IDC_RADIO4                      1007
#define IDC_RADIO5                      1008
#define IDC_RADIO1                      1009
#define IDC_RADIO2                      1010
#define ID_32786                        32786
#define ID_32787                        32787
#define ID_THEO32788                    32788
#define ID_THEO32789                    32789
#define ID_THEO32790                    32790
#define ID_THEONG32791                  32791
#define ID_THEONG32792                  32792
#define ID_THEONG32793                  32793
#define ID_EXIT32794                    32794
#define ID_DOITENTHEOCREATED            32795
#define ID_DOITENTHEOMODIFIED           32796
#define ID_DOITENTHEOTAKEN              32797
#define ID_DOITENTHEOTRUOC              32798
#define ID_DOITENTHEOGIUA               32799
#define ID_DOITENTHEOSAU                32800
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32801
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
